package com.gamaza.tfgamaza.esper.transformers.thingspeak.components;

/**
 * Channel class
 */
public class Channel {

	/** Private variables **/
	
	private Integer id;
	private String name;
	private String description;
	private Double latitude, longitude;
	private String field1 = "";
	private String field2 = "";
	private String field3 = "";
	private String field4 = "";
	private String field5 = "";
	private String field6 = "";
	private String field7 = "";
	private String field8 = "";
	private String created_at, updated_at;
	private Integer last_entry_id;
	
	/** Getters and Setters **/
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public String getField1() {
		return field1;
	}
	
	public void setField1(String field1) {
		this.field1 = field1;
	}
	
	public String getField2() {
		return field2;
	}
	
	public void setField2(String field2) {
		this.field2 = field2;
	}
	
	public String getField3() {
		return field3;
	}
	
	public void setField3(String field3) {
		this.field3 = field3;
	}
	
	public String getField4() {
		return field4;
	}
	
	public void setField4(String field4) {
		this.field4 = field4;
	}
	
	public String getField5() {
		return field5;
	}
	
	public void setField5(String field5) {
		this.field5 = field5;
	}
	
	public String getField6() {
		return field6;
	}
	
	public void setField6(String field6) {
		this.field6 = field6;
	}
	
	public String getField7() {
		return field7;
	}
	
	public void setField7(String field7) {
		this.field7 = field7;
	}
	
	public String getField8() {
		return field8;
	}
	
	public void setField8(String field8) {
		this.field8 = field8;
	}
	
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	
	public String getUpdated_at() {
		return updated_at;
	}
	
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	
	public Integer getLast_entry_id() {
		return last_entry_id;
	}
	
	public void setLast_entry_id(Integer last_entry_id) {
		this.last_entry_id = last_entry_id;
	}

	@Override
	public String toString() {
		return "Channel [id=" + id + ", name=" + name + ", description=" + description + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", field1=" + field1 + ", field2=" + field2 + ", field3=" + field3
				+ ", field4=" + field4 + ", field5=" + field5 + ", field6=" + field6 + ", field7=" + field7
				+ ", field8=" + field8 + ", created_at=" + created_at + ", updated_at=" + updated_at
				+ ", last_entry_id=" + last_entry_id + "]";
	}
	
}
