package com.gamaza.tfgamaza.esper.transformers.mqtt;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.ContainsTransformerMethods;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ContainsTransformerMethods
public class MqttTransformer extends AbstractMessageTransformer{

	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(MqttTransformer.class);
	
	@Override
	@SuppressWarnings("unchecked")
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		/** Map the payload content **/
		Map<String, Object> eventPayload = null;
		try {
			eventPayload = new ObjectMapper().readValue(message.getPayloadAsString(), Map.class);
		} catch (JsonParseException e) {e.printStackTrace();} 
		catch (JsonMappingException e) {e.printStackTrace();} 
		catch (IOException e) {e.printStackTrace();} 
		catch (Exception e) {e.printStackTrace();}
		
		//Add alert properties
		eventPayload.put("level", 0);
		eventPayload.put("property", "");
		
		/** Show in log **/
		LOGGER.info(String.format("%60c", ' ').replace(' ', '*') + " MQTT Event " + String.format("%66c", ' ').replace(' ', '*'));
		return eventPayload;
	}

}
