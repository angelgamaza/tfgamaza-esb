package com.gamaza.tfgamaza.esper.transformers.thingspeak;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.ContainsTransformerMethods;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamaza.tfgamaza.esper.transformers.thingspeak.components.ThingSpeak;

/**
 * ThingSpeak Event Transformer
 */
@ContainsTransformerMethods
public class ThingSpeakTransformer extends AbstractMessageTransformer{
	
	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(ThingSpeakTransformer.class);

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		//Map ThingSpeak JSON to ThingSpeak Class Template
		ThingSpeak eventObject = new ThingSpeak();
		try {
			eventObject = new ObjectMapper().readValue(message.getPayloadAsString(), ThingSpeak.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Create the main structure to allocate the data
		Map<String, Object> eventMap = createEventStructure(eventObject);
		
		//Show in log
		LOGGER.info(String.format("%60c", ' ').replace(' ', '*') + " ThingSpeak Event " + String.format("%60c", ' ').replace(' ', '*'));
		return eventMap;
	}
	
	/**
	 * @param eventObject
	 * @return
	 */
	private Map<String, Object> createEventStructure(ThingSpeak eventObject){
		
		/** Location data **/
		Map<String, Object> location = new LinkedHashMap<String, Object>();
		location.put("latitude", eventObject.getChannel().getLatitude());
		location.put("longitude", eventObject.getChannel().getLongitude());

		/** Event data Map **/ 
		Map<String, Object> eventDataMap = new LinkedHashMap<String, Object>();
		
		//Common data.
		eventDataMap.put("id", eventObject.getChannel().getId());
		eventDataMap.put("name", eventObject.getChannel().getName());
		if(!eventObject.getChannel().getDescription().isEmpty())
			eventDataMap.put("description", eventObject.getChannel().getDescription());
		eventDataMap.put("timestamp", eventObject.getChannel().getCreated_at());
		eventDataMap.put("location", location);
		
		//Particular data.
		if(!eventObject.getChannel().getField1().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField1(), eventObject.getFeeds().get(0).getField1());
		if(!eventObject.getChannel().getField2().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField2(), eventObject.getFeeds().get(0).getField2());
		if(!eventObject.getChannel().getField3().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField3(), eventObject.getFeeds().get(0).getField3());
		if(!eventObject.getChannel().getField4().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField4(), eventObject.getFeeds().get(0).getField4());
		if(!eventObject.getChannel().getField5().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField5(), eventObject.getFeeds().get(0).getField5());
		if(!eventObject.getChannel().getField6().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField6(), eventObject.getFeeds().get(0).getField6());
		if(!eventObject.getChannel().getField7().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField7(), eventObject.getFeeds().get(0).getField7());
		if(!eventObject.getChannel().getField8().isEmpty())
			eventDataMap.put(eventObject.getChannel().getField8(), eventObject.getFeeds().get(0).getField8());
		
		//Add alert properties
		eventDataMap.put("level", 0);
		eventDataMap.put("property", "");
		
		return eventDataMap;
	}

}
