package com.gamaza.tfgamaza.esper.transformers.thingspeak.components;

import java.util.List;

/**
 * ThingSpeak class
 */
public class ThingSpeak {
	
	/** Private variables **/
	private Channel channel;
	private List<Feed> feeds;
	
	/** Getters and Setters **/
	
	public Channel getChannel() {
		return channel;
	}
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	public List<Feed> getFeeds() {
		return feeds;
	}
	public void setFeeds(List<Feed> feeds) {
		this.feeds = feeds;
	}
	@Override
	public String toString() {
		return "ThingSpeak [channel=" + channel + ", feeds=" + feeds + "]";
	}
	
}
