package com.gamaza.tfgamaza.esper.transformers.thingspeak.components;

/**
 * Feed class
 */
public class Feed {
	
	/** Private variables **/
	
	private Double field1 = 0.0, field2 = 0.0, field3 = 0.0, field4 = 0.0, field5 = 0.0, field6 = 0.0, field7 = 0.0, field8 = 0.0;
	private String created_at;
	private Integer entry_id;
	
	/** Getters and Setters **/
	
	public Double getField1() {
		return field1;
	}
	
	public void setField1(Double field1) {
		this.field1 = field1;
	}
	
	public Double getField2() {
		return field2;
	}
	
	public void setField2(Double field2) {
		this.field2 = field2;
	}
	
	public Double getField3() {
		return field3;
	}
	
	public void setField3(Double field3) {
		this.field3 = field3;
	}
	
	public Double getField4() {
		return field4;
	}
	
	public void setField4(Double field4) {
		this.field4 = field4;
	}
	
	public Double getField5() {
		return field5;
	}
	
	public void setField5(Double field5) {
		this.field5 = field5;
	}
	
	public Double getField6() {
		return field6;
	}
	
	public void setField6(Double field6) {
		this.field6 = field6;
	}
	
	public Double getField7() {
		return field7;
	}
	
	public void setField7(Double field7) {
		this.field7 = field7;
	}
	
	public Double getField8() {
		return field8;
	}
	
	public void setField8(Double field8) {
		this.field8 = field8;
	}
	
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	
	public Integer getEntry_id() {
		return entry_id;
	}
	
	public void setEntry_id(Integer entry_id) {
		this.entry_id = entry_id;
	}

	@Override
	public String toString() {
		return "Feed [field1=" + field1 + ", field2=" + field2 + ", field3=" + field3 + ", field4=" + field4
				+ ", field5=" + field5 + ", field6=" + field6 + ", field7=" + field7 + ", field8=" + field8
				+ ", created_at=" + created_at + ", entry_id=" + entry_id + "]";
	}
	
}
