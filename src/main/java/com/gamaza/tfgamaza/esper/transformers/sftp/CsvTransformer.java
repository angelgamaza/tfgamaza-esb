package com.gamaza.tfgamaza.esper.transformers.sftp;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.ContainsTransformerMethods;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

@ContainsTransformerMethods
public class CsvTransformer extends AbstractMessageTransformer{
	
	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(CsvTransformer.class);
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		
		/** Get events List from CSV **/
		List<Map<String, Object>> returnList = null;
		try {
			returnList = getCSVRows(message.getPayloadAsString());
		} catch (Exception e) {e.printStackTrace();}
		
		/** Show in log **/
		LOGGER.info(String.format("%60c", ' ').replace(' ', '*') + " SFTP CSV Reception " + String.format("%58c", ' ').replace(' ', '*'));
		LOGGER.info("Received CSV from Junta at " + DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(LocalDateTime.now()));
		
		return returnList;
	}
	
	/**
	 * Map all CSV Rows to List
	 * @param csvContent
	 * @return SftpJson
	 * @throws IOException
	 */
	private List<Map<String, Object>> getCSVRows(String csvContent) throws IOException{
		
		/** Structure to allocate CSV **/
		MappingIterator<Map<String, Object>> csvMappedContent = null;
		try {
			csvMappedContent = new CsvMapper().reader(Map.class).with(CsvSchema.emptySchema().withHeader()).readValues(csvContent);
		} catch (Exception e) {e.printStackTrace();}
		
		return csvMappedContent.readAll();
	}

}
