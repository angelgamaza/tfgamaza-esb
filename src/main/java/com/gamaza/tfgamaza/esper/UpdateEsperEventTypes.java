package com.gamaza.tfgamaza.esper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.mule.api.annotations.param.Payload;

import com.gamaza.tfgamaza.esper.tools.EsperTfgApi;

/**
 * Checks if one or more Event Type have been deleted in database and updates them in Esper
 */
public class UpdateEsperEventTypes {

	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(UpdateEsperEventTypes.class);
	
	/**
	 * Action method
	 * @param databaseEventTypeList
	 */
	public synchronized void checkDeletedOrUpdatedEventTypes(@Payload List<Map<String, Object>> databaseEventTypeList){
		
		//List of Event Types that are stored in database currently
		List<String> databaseEventTypes = databaseEventTypeList.stream().map(eventType -> eventType.get("name").toString()).collect(Collectors.toList());
		
		//List of Event Types that are in Esper currently
		List<String> esperEventTypeList = EsperTfgApi.getAllEventTypesList();
		//Delete Event Types that are not stored in database
		esperEventTypeList.remove("JuntaAndalucia");
		esperEventTypeList.remove("MQTT");
		
		//List of EPL Event Patterns that are in Esper currently
		List<String> esperEplEventPatternsList = EsperTfgApi.getAllEplEventPatternsList();
		
		//Delete Event Types in Esper that have been deleted in database
		esperEventTypeList.forEach(eventType -> {
			if(!databaseEventTypes.contains(eventType)){
				//Delete linked EPL Event Patterns
				esperEplEventPatternsList.forEach(eplEventPattern -> {
					if(EsperTfgApi.getEplEventPatternContent(eplEventPattern).contains(eventType))
						EsperTfgApi.deleteEplEventPattern(eplEventPattern);
				});

				/** Logs and event type deletion **/
				LOGGER.info(String.format("%60c", ' ').replace(' ', '*') + " Event Type Change " + String.format("%59c", ' ').replace(' ', '*'));
				LOGGER.info("Event type " + eventType + " deleted, deleting in Esper...");
				EsperTfgApi.deleteEventType(eventType);
				LOGGER.info("Event type " + eventType + " deleted in Esper");
			}
		});
	}

}
