package com.gamaza.tfgamaza.esper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.mule.api.annotations.param.Payload;

import com.gamaza.tfgamaza.esper.tools.EsperTfgApi;

/**
 * Checks if one or more EPL Event Pattern have been deleted in database and updates them in Esper
 */
public class UpdateEsperDeployedEplEventPatterns {

	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(UpdateEsperDeployedEplEventPatterns.class);

	/**
	 * Checks if one or more patterns have been updated or deleted in database and it updates them in Esper
	 * @param databaseEplEventPatternsList
	 */
	public synchronized void checkDeletedOrUpdatedEplEventPatterns(@Payload List<Map<String, Object>> databaseEplEventPatternsList){
		
		//Gets an array of EPL Event Patterns from Esper
		List<String> esperEplEventPatterns = EsperTfgApi.getAllEplEventPatternsList();
		//Gets all deployed EPL Event Patterns from database
		List<String> databaseEplEventPatterns = databaseEplEventPatternsList.stream().map(eplEventPattern -> eplEventPattern.get("name").toString()).collect(Collectors.toList());
		
		//If database EPL Event Patterns list not contains some EPL Event Pattern, it will be deleted 
		esperEplEventPatterns.forEach(eplEventPattern -> {
			if(!databaseEplEventPatterns.contains(eplEventPattern)){
				LOGGER.info(String.format("%60c", ' ').replace(' ', '*') + " EPL Event Pattern change " + String.format("%62c", ' ').replace(' ', '*'));
				LOGGER.info("EPL Event Pattern " + eplEventPattern + " deleted, deleting in Esper...");
				EsperTfgApi.deleteEplEventPattern(eplEventPattern);
				LOGGER.info("EPL Event Pattern " + eplEventPattern + " deleted in Esper");
				
			}
		});
		
	}

}
