package com.gamaza.tfgamaza.esper.tools;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/esper")
public class EsperTfgRestApi {
	
	/**
	 * Gets and returns all the Event Types in Esper Engine
	 * @return eplEventPattern list
	 */
	@Path("/event_types")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<String> getAllEventTypes(){
		return EsperTfgApi.getAllEventTypesList();
	}

	/**
	 * Gets and returns all the EPL Event Patterns in Esper Engine
	 * @return eventType list
	 */
	@Path("/epl_event_patterns")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<String> getAllEplEventPatterns(){
		return EsperTfgApi.getAllEplEventPatternsList();
	}

}
