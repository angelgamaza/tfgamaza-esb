package com.gamaza.tfgamaza.esper.tools;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/*
 * Sends to Esper one Event Type (Subroutine)
 */
@SuppressWarnings("unchecked")
public class EsperSendEvent {

	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(EsperSendEvent.class);

	/**
	 * Send Event to Esper Engine (Create the Event Type if not exists)
	 * @param eventMap
	 * @param eventTypeName
	 */
	public static void sendEvent(Map<String, Object> eventMap, String eventTypeName){
		
		//Add Event Type to Esper if not exists
		if (!EsperTfgApi.eventTypeExists(eventTypeName)) {
			EsperTfgApi.addEventType(eventTypeName, getEventTypeStructure(eventMap));
			LOGGER.info(eventTypeName + " event type added to Esper engine");
		}
		
		//Send event to Esper
		EsperTfgApi.sendEventType(eventMap, eventTypeName);
		LOGGER.info("Sending event '" + eventTypeName + "' to Esper: " + eventMap);
	}
	
	/**
	 * Create the Event Type Structure (With java types)
	 * @param eventMap
	 * @return eventTypeMap
	 */
	private static Map<String, Object> getEventTypeStructure(Map<String, Object> eventMap) {
		
		// LinkedHashMap will iterate in the order in which the entries were put into the map.
		Map<String, Object> eventTypeMap = new LinkedHashMap<String, Object>();
		eventMap.keySet().forEach(key -> {
			Object value = eventMap.get(key);
			if (value instanceof Map) 
				eventTypeMap.put(key, getEventTypeStructure((Map<String, Object>) value));
			else 
				eventTypeMap.put(key, value.getClass());
		});
		return eventTypeMap;
	}
	
}
