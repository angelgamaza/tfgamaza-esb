package com.gamaza.tfgamaza.esper;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.gamaza.tfgamaza.esper.tools.EsperSendEvent;

/**
 * JuntaAndalucia Event Types Sender 
 */
@SuppressWarnings("unchecked")
public class SendJuntaEventToEsperComponent implements Callable{
	
	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(SendJuntaEventToEsperComponent.class);

	@Override
	public Object onCall(final MuleEventContext eventContext) throws Exception { 
		
		/** Get the Event Type Map **/
		Map<String, Object> eventPayload = (Map<String, Object>) eventContext.getMessage().getPayload();
		
		//Add alert properties
		eventPayload.put("level", 0);
		eventPayload.put("property", "");

		//Show in log
		LOGGER.info(String.format("%60c", ' ').replace(' ', '*') + " JuntaAndalucia Event " + String.format("%60c", ' ').replace(' ', '*'));
		
		//Send the event to Esper Engine
		EsperSendEvent.sendEvent(eventPayload, "JuntaAndalucia");

		return new JSONObject(eventPayload);
	}
}
