package com.gamaza.tfgamaza.esper;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.StatementAwareUpdateListener;
import com.gamaza.tfgamaza.esper.tools.EsperTfgApi;


/**
 * Add EPL Event Pattern to Esper Engine and listen Complex Events
 */
@SuppressWarnings("unchecked")
public class AddEplEventPatternToEsperComponent implements Callable { 

	/** Logger to show information log **/
	private static final Logger LOGGER = Logger.getLogger(AddEplEventPatternToEsperComponent.class);

	@Override
	public Object onCall(final MuleEventContext eventContext) throws Exception {

		final Map<String, Object> payload = (Map<String, Object>) eventContext.getMessage().getPayload();
		final EPStatement eplEventPattern = EsperTfgApi.addEplEventPattern(payload.get("content").toString());		

		StatementAwareUpdateListener genericListener = new StatementAwareUpdateListener() {

			@Override
			public void update(EventBean[] newComplexEvents, EventBean[] oldComplexEvents, EPStatement detectedEventPattern, EPServiceProvider serviceProvider) {

				if (newComplexEvents != null) {	

					try {
						/** newComplexEvents[0].getUnderlying() is the payload of the complex event detected by this listener.
							detectedEventPattern.getEventType().getName() specifies the event pattern that has created this complex event. **/
						String eventPatternName = detectedEventPattern.getEventType().getName();
						
						/** Create the detected complex event as a Java map  **/
						Map<String, Object> complexEvent = (Map<String, Object>) newComplexEvents[0].getUnderlying();
						complexEvent.put("name", eventPatternName);
						/** Create the Mule message containing the complex event to be sent to the ComplexEventReceptionAndDecisionMaking flow **/			
						MuleMessage message = new DefaultMuleMessage(complexEvent, eventContext.getMuleContext());

						/** Add messageProperties, a map containing eventPatternName, to the Mule message **/
						Map<String, Object> messageProperties = new HashMap<String, Object>();
						messageProperties.put("eventPatternName", eventPatternName);
						message.addProperties(messageProperties, PropertyScope.OUTBOUND);

						/** Send the created Mule message to the ComplexEventConsumerGlobalVM connector located in the ComplexEventReceptionAndDecisionMaking flow **/
						eventContext.getMuleContext().getClient().dispatch("ComplexEventConsumerGlobalVM", message);
					} catch (MuleException e) {e.printStackTrace();}
				}
			}
		};

		eplEventPattern.addListener(genericListener);
		
		/** Show log info **/
		LOGGER.info(String.format("%60c", ' ').replace(' ', '*') + " EPL Event Pattern addition " + String.format("%60c", ' ').replace(' ', '*'));
		LOGGER.info("EPL Event Pattern " + payload.get("name").toString() + " added to Esper successfully");

		return payload;
	}	
}
