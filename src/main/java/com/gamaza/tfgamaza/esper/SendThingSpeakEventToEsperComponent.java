package com.gamaza.tfgamaza.esper;


import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.gamaza.tfgamaza.esper.tools.EsperSendEvent;

/*
 * ThingSpeak Event Types Sender
 */
@SuppressWarnings("unchecked")
public class SendThingSpeakEventToEsperComponent implements Callable{

	@Override
	public Object onCall(final MuleEventContext eventContext) throws Exception {
		
		/** Get the Event Type Map **/
		Map<String, Object> eventPayload = (Map<String, Object>) eventContext.getMessage().getPayload();
		
		//Send the event to Esper Engine
		EsperSendEvent.sendEvent(eventPayload, (String) eventPayload.get("name"));
		
		return new JSONObject(eventPayload);
	}
	
}
