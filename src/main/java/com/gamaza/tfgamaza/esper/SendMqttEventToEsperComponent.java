package com.gamaza.tfgamaza.esper;

import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.gamaza.tfgamaza.esper.tools.EsperSendEvent;

/**
 * MQTT Event Types Sender
 */
@SuppressWarnings("unchecked")
public class SendMqttEventToEsperComponent implements Callable{
	
	@Override
	public Object onCall(final MuleEventContext eventContext) throws Exception { 
		
		/** Get the Event Type Map **/
		Map<String, Object> eventPayload = (Map<String, Object>) eventContext.getMessage().getPayload();

		//Send the event to Esper Engine
		EsperSendEvent.sendEvent(eventPayload, "MQTT");

		return new JSONObject(eventPayload);
	}

}
