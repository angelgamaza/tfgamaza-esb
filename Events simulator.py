import requests, json, time, random
from random import randint

# Field values
air_values = [0, 0, 0, 0, 0, 0, 0]
water_values = [0, 0]

# Execute forever
while 1:
	# Moderated values (Air)
	air_values[0] = round(random.uniform(0.00, 0.054), 3)
	air_values[1] = round(random.uniform(0.00, 35.4), 1)
	air_values[2] = randint(0, 154)
	air_values[3] = round(random.uniform(0.00, 9.4), 1)
	air_values[4] = randint(0, 75)
	air_values[5] = randint(0, 100)
	air_values[6] = round(random.uniform(10, 31.4), 1)

	# Moderated values (Water)
	water_values[0] = round(random.uniform(0, 7), 1)
	water_values[1] = round(random.uniform(0, 2), 3)

	# Possible dangerous values
	probability = randint(1, 10)
	if probability > 9:

		# Air values
		particular_probability = randint(1, 6)
		if particular_probability == 1:	
			air_values[0] = round(random.uniform(0.00, 0.604), 3)
		if particular_probability == 2:	
			air_values[1] = round(random.uniform(0.00, 500.4), 1)
		if particular_probability == 3:	
			air_values[2] = randint(0, 604)
		if particular_probability == 4:	
			air_values[3] = round(random.uniform(0.00, 50.4), 1)
		if particular_probability == 5:	
			air_values[4] = randint(0, 1004)
		if particular_probability == 6:	
			air_values[5] = randint(0, 2049)
		if particular_probability == 7:	
			air_values[6] = round(random.uniform(14, 42), 1)

		# Water values
		water_values[0] = round(random.uniform(0, 14), 1)
		water_values[1] = round(random.uniform(0, 4), 3)
	
	# Build JSON	
	headers = {'content-type': 'application/json'}
	air_data = {'field1': air_values[0], 'field2': air_values[1], 'field3': air_values[2], 'field4': air_values[3], 'field5': air_values[4], 'field6': air_values[5], 'field7': air_values[6]}
	water_data = {'field1': water_values[0], 'field2': water_values[1]}

	# Send POST Request and Log information
	print("Sending data to ThingSpeak channel 170892:")
	print(air_values)
	print("Result: ", requests.post('https://api.thingspeak.com/update?api_key=Q303Y0A47H37EOBY', data=json.dumps(air_data), headers=headers).status_code)
	print("Sending data to ThingSpeak channel 545022:")
	print(water_values)
	print("Result: ", requests.post('https://api.thingspeak.com/update?api_key=3NTCQZO01JEMZ6U7', data=json.dumps(water_data), headers=headers).status_code)

	# Delay
	time.sleep(2)

